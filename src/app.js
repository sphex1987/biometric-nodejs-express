const path = require('path')
const express = require('express')
const axios = require('axios')
const CircularJSON = require('circular-json');
var FormData = require('form-data');
var fs = require('fs');
require('custom-env').env()
const token = '';
const app = express()
const formidable = require('formidable')
const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.static(publicDirectoryPath))
app.use(express.json())

app.get('', (req, res) => {
 res.send('<h1>Home</h1>')
})
app.post('/bio', (req, res) => {

//	new formidable.IncomingForm()

	if(process.env.CLIENT_ID == '') {
		res.send('Please set your CLIENT_ID in ENV file');
	}

	if(process.env.CLIENT_SECRET == '') {
		res.send('Please set your CLIENT_SECRET in ENV file');
	}

	if(process.env.X_TOKEN == '') {
		res.send('Please set your X_TOKEN in ENV file');
	}

    var form = new FormData();
    //var form;

  new formidable.IncomingForm().parse(req, (err, fields, files) => {
    if (err) {
      console.error('Error', err)
      throw err
    }
    //console.log('Fields', fields)
    //console.log('Files', files)
    var counter = 0
    for (const [key, value] of Object.entries(files)) {

      var send_img = fs.readFileSync(value.path);
      
      if(counter == 0) {
      	form.append("image1", fs.createReadStream(value.path), { knownLength: fs.statSync(value.path).size });
      } else {
	  	form.append("image2", fs.createReadStream(value.path), { knownLength: fs.statSync(value.path).size });
	  }
	  
      counter++
    }


  })


	axios
	  .post('https://bvengine.com/oauth/token', {
	    client_id: process.env.CLIENT_ID,
	    client_secret: process.env.CLIENT_SECRET,
	    grant_type: 'client_credentials'
	  })
	  .then(response => {

	  	var temp_token = CircularJSON.stringify(response.data.access_token)
	  	var temp_token = temp_token.replace('"','')

		axios.post('https://bvengine.com/api/compare', form, {
		  headers: {
		    Authorization: 'Bearer ' + temp_token,
		    'Content-Type': `multipart/form-data; boundary=${form._boundary}`,
		    'X-Token': process.env.X_TOKEN,
		    'Accept': 'application/json',
		    'Content-Length': form.getLengthSync()
		  }
		})	 
		.then(response => {
			console.log(response);
			res.json( response.data );
		})   
		.catch(error => {
	    	console.error(error)
	  	})

	 })  
})

app.listen(3000, () => {
 console.log('Server is up on port 3000.')
}) 